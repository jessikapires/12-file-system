# 11 - File System

O File System (`fs`) é uma das grandes APIs do NodeJS e com ela podemos manipular qualquer tipo de arquivo. Criar, editar e até deletar.

https://nodejs.org/api/fs.html#fs_file_system

## fs.writeFile

Método que cria arquivos. 

```
const data = new Uint8Array(Buffer.from('Hello Node.js'));
fs.writeFile('message.txt', data, (err) => {
  if (err) throw err;
  console.log('The file has been saved!');
});
```

https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback

## fs.readFile

Método que le arquivos. 

```
const data = new Uint8Array(Buffer.from('Hello Node.js'));
fs.writeFile('message.txt', data, (err) => {
  if (err) throw err;
  console.log('The file has been saved!');
});
```

## fs.appendFile

Método que adiciona um conteúdo ao final do arquivo. 

```
fs.appendFile('message.txt', 'data to append', (err) => {
  if (err) throw err;
  console.log('The "data to append" was appended to file!');
});
```

https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback

# Desafio

No desafio dessa semana você deverá criar um Endpoint GET e um Endpoint POST, neles você usará a mesma ideia de criação e consulta no MD, a diferença é que vocês terão que usar o array do arquivo `sample.json` como REFERENCIA.

Endpoint GET `/buscar`: Deverá verificar se no arquivo `cache.json` há o registro que esteja sendo buscado, se o registro não estiver no arquivo, deve ser feito a requisição para o MD buscando com os mesmos parametros enviados para o Endpoint. Ao final, o resultado deve ser guardado no arquivo `cache.json`.
- Verificar se existe no arquivo `cache.json`
- Buscar pelos parametros: "Nome", "Idade" ou "Email"
- Guardar o resultado no `cache.json`

Endpoint POST `/criar`: Deverá verificar se o registro com os mesmos valores já existe no Master Data e se não existir cria-lo. Por final, adicionar no `cache.json`
- Verificar se existe no arquivo `cache.json`
- Verificar se o registro existe no Master Data
- Guardar os dados no `cache.json`

*Todos os dados guardados no `cache.json` DEVEM conter o ID do registro*

Para se destacar: Crie *outro* Endpoint POST `/em-massa`. Ele deverá ler o arquivo `sample.json` e criar cada um dos registros dele enviando para o seu Endpoint POST que foi explicado acima. 
- Iterar o arquivo `sample.json` e
- Enviar cada registro para o Endpoint POST `/criar`

IMPORTANTE SE REALIZAR O DESAFIO DE DESTAQUE: Renomeie no arquivo `sample.json` TODAS as ocorrencias do texto "Nome" e "nome" pelo seu nome com todas as letras em minúsculo.  

Campos da entidade AL a serem preenchidos:

- `nome`: Nome - string
- `email`: E-mail - string
- `idade`: Idade - int